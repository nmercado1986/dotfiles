set nocompatible
filetype off                  " required

call plug#begin("$HOME/.vim/plugged")

" ==================== PLUGINS ====================

Plug 'VundleVim/Vundle.vim'

Plug 'AndrewRadev/splitjoin.vim'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
" Plug 'Townk/vim-autoclose'
" Plug 'Valloric/MatchTagAlways'
Plug 'airblade/vim-gitgutter'
Plug 'altercation/vim-colors-solarized'
Plug 'b4winckler/vim-angry'
Plug 'christoomey/vim-tmux-navigator'
Plug 'henrik/vim-indexed-search'
Plug 'idanarye/vim-merginal'
Plug 'joonty/vdebug'
Plug 'kien/ctrlp.vim'
Plug 'majutsushi/tagbar'
Plug 'mattn/emmet-vim'
Plug 'mbbill/undotree'
Plug 'mileszs/ack.vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'scrooloose/syntastic'
Plug 'szw/vim-g'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'StanAngeloff/php.vim'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'

"All of your Plugins must be added before the following line
call plug#end()

" ==================== SESSIONS ====================
let g:session_autosave = "no"

" ==================== GVIM ====================

if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window.
  set lines=999 columns=999
endi

" ==================== OPCIONES  ====================
"
"set showcmd     "show incomplete cmds down the bottom
"set showmode    "show current mode down the bottom
"
set number              "show line numbers
set relativenumber      "show line numbers
"
""allow backspacing over everything in insert mode
set backspace=indent,eol,start

set gdefault
set incsearch   "find the next match as we type the search
set hlsearch    "hilight searches by default

set wrap        "dont wrap lines
set linebreak   "wrap lines at convenient points

set cursorline

"load ftplugins and indent files
filetype plugin indent on

"default indent settings
set cindent
set shiftwidth=4
set softtabstop=4
set expandtab

"folding settings
set foldmethod=indent   "fold based on indent
set foldnestmax=3       "deepest fold is 3 levels
set nofoldenable        "dont fold by default

set formatoptions-=o "dont continue comments when pushing o/O

"vertical/horizontal scroll off settings
set scrolloff=3
set sidescrolloff=7
set sidescroll=1

""allow backspacing over everything in insert mode
set backspace=indent,eol,start

"hide buffers when not displayed
set hidden

set ignorecase
set smartcase


"==================== ARCHIVOS  ====================


if v:version >= 703
    "undo settings
    set undodir=~/.vim/undofiles
    set backupdir=~/.vim/backup
    set directory=~/.vim/backup
    set viminfofile=~/.vim/viminfo
    set undofile

    set colorcolumn=+1 "mark the ideal max text width
endif

"==================== COLORES  ====================

"turn on syntax highlighting
syntax on

"tell the term has 256 colors
set t_Co=16
let g:solarized_termcolors=16
let g:solarized_contrast="high"
set background=dark
colorscheme solarized

"==================== STATUSLINE  ====================

"statusline setup
set statusline =%#identifier#
set statusline+=[%f]    "tail of the filename
set statusline+=%*

"display a warning if fileformat isnt unix
set statusline+=%{&ff!='unix'?'['.&ff.']':''}
set statusline+=%*

"display a warning if file encoding isnt utf-8
set statusline+=%#warningmsg#
set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}
set statusline+=%*

"read only flag
set statusline+=%#identifier#
set statusline+=%r
set statusline+=%*

set statusline+=%{fugitive#statusline()}

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

set statusline+=%=      "left/right separator
set statusline+=%c,     "cursor column
set statusline+=%l/%L   "cursor line/total lines
set statusline+=\ %P    "percent through file
set laststatus=2

"==================== NERDTREE  ====================

let NERDTreeHijackNetrw = 0
let g:NERDTreeQuitOnOpen = 1
let g:NERDTreeMouseMode = 2
let g:NERDTreeWinSize = 40
let g:NERDTreeChDirMode=2
let g:NERDTreeMinimalUI=1
let NERDTreeShowLineNumbers=1
autocmd FileType nerdtree setlocal relativenumber

"==================== MAPPINGS  ====================
"
let mapleader=" "
"explorer mappings
nnoremap <C-f> :CtrlP<cr>
nnoremap <C-t> :Tags<CR>
nnoremap <C-g> :Commits<CR>
nnoremap q; :History:<cr>
nnoremap  <Leader>f :NERDTreeToggle<cr>
nnoremap  <Leader>t :TagbarOpenAutoClose<CR>
nnoremap  <Leader>m :Merginal<CR>
nnoremap  <Leader>g :Gstatus<CR>
nnoremap  <Leader>s :Git stash save<CR>
nnoremap  <Leader>p :Git stash pop<CR>
nnoremap  <Leader>d :Gdiff<CR>
nnoremap  <Leader>o :only<CR>
nnoremap  <Leader>q :close<CR>
nnoremap  <Leader>v :vnew<CR>
nnoremap  <Leader>n :new<CR>
nnoremap  <leader>w :call SwitchDiffW()<CR>
command  Gc  Gcommit
command  G  Google
nnoremap - /
nnoremap d- d/
nnoremap y- y/
nnoremap c- c/
nnoremap dv- dv/
nnoremap yv- yv/
nnoremap cv- cv/
nnoremap _ ?
nnoremap d_ d?
nnoremap y_ y?
nnoremap c_ c?
nnoremap dv_ dv?
nnoremap yv_ yv?
nnoremap cv_ cv?
noremap Q q:
nnoremap Y y$

"==================== CTRLP  ====================

" Ignore some folders and files for CtrlP indexing
let g:ctrlp_custom_ignore = {
            \ 'dir':  '\.git$\|\.yardoc\|\.public$|log\|tmp$',
            \ 'file': '\.so$\|\.dat$|\.DS_Store$'
            \ }

let g:ctrlp_clear_cache_on_exit=0
let g:ctrlp_working_path_mode = 0

"==================== FUNCIONES VARIAS  ====================
"
"jump to last cursor position when opening a file
"dont do it when writing a commit log entry
autocmd BufReadPost * call SetCursorPosition()
function! SetCursorPosition()
    if &filetype !~ 'svn\|commit\c'
        if line("'\"") > 0 && line("'\"") <= line("$")
            exe "normal! g`\""
            normal! zz
        endif
    else
        call cursor(1,1)
    endif
endfunction


set tabline=%!MyTabLine()  " custom tab pages line
function MyTabLine()
    let s = '' " complete tabline goes here
    " loop through each tab page
    for t in range(tabpagenr('$'))
        " set highlight
        if t + 1 == tabpagenr()
            let s .= '%#TabLineSel#'
        else
            let s .= '%#TabLine#'
        endif
        " set the tab page number (for mouse clicks)
        let s .= '%' . (t + 1) . 'T'
        let s .= ' '
        " set page number string
        let s .= t + 1 . ' '
        " get buffer names and statuses
        let n = ''      "temp string for buffer names while we loop and check buftype
        let m = 0       " &modified counter
        let bc = len(tabpagebuflist(t + 1))     "counter to avoid last ' '
        " loop through each buffer in a tab
        for b in tabpagebuflist(t + 1)
            " buffer types: quickfix gets a [Q], help gets [H]{base fname}
            " others get 1dir/2dir/3dir/fname shortened to 1/2/3/fname
            if getbufvar( b, "&buftype" ) == 'help'
                let n .= '[H]' . fnamemodify( bufname(b), ':t:s/.txt$//' )
            elseif getbufvar( b, "&buftype" ) == 'quickfix'
                let n .= '[Q]'
            else
                let n .= pathshorten(bufname(b))
            endif
            " check and ++ tab's &modified count
            if getbufvar( b, "&modified" )
                let m += 1
            endif
            " no final ' ' added...formatting looks better done later
            if bc > 1
                let n .= ' '
            endif
            let bc -= 1
        endfor
        " add modified label [n+] where n pages in tab are modified
        if m > 0
            let s .= '[' . m . '+]'
        endif
        " select the highlighting for the buffer names
        " my default highlighting only underlines the active tab
        " buffer names.
        if t + 1 == tabpagenr()
            let s .= '%#TabLineSel#'
        else
            let s .= '%#TabLine#'
        endif
        " add buffer names
        if n == ''
            let s.= '[New]'
        else
            let s .= n
        endif
        " switch to no underlining and add final space to buffer list
        let s .= ' '
    endfor
    " after the last tab fill with TabLineFill and reset tab page nr
    let s .= '%#TabLineFill#%T'
    " right-align the label to close the current tab page
    if tabpagenr('$') > 1
        let s .= '%=%#TabLineFill#%999Xclose'
    endif
    return s
endfunction

"==================== SYNTASTIC  ====================

"syntastic settings
let syntastic_stl_format = '[Syntax: %E{line:%fe }%W{#W:%w}%B{ }%E{#E:%e}]'
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_php_checkers = ['php']


let g:syntastic_javascript_checkers = ['jshint']

" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
" let g:fzf_history_dir = '~/.local/share/fzf-history'

"==================== COMMENTARY ====================
autocmd FileType php setlocal commentstring=\/\/\ %s




" ==================== AG ====================
let g:ackprg="ag --vimgrep "
let g:ack_use_dispatch=1
let g:ack_default_options=" -k -s -H --nocolor --nogroup --column"
let g:ack_use_cword_for_empty_search=1

" ==================== DEBUG  ====================
let g:vdebug_options = {}
let g:vdebug_options["port"] = 9000
let g:vdebug_options["path_maps"] = {
\    "/vagrant/batch/": "C:\\Users\\nmercado\\Documents\\Code\\lr\\batch\\",
\    "/vagrant/web/": "C:\\Users\\nmercado\\Documents\\Code\\lr\\web\\",
\    "/vagrant/eAgent/": "C:\\Users\\nmercado\\Documents\\Code\\lr-eagent\\eAgent\\"
\}

" ==================== FZF ====================
" This is the default extra key bindings
" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%' }

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" ==================== TAGBAR ====================

let g:tagbar_type_php = {
            \ 'kinds'     : [
            \ 'c:classes:0:0',
            \ 'd:constants:0:0',
            \ 'f:functions',
            \ 'i:interfaces',
            \ ],
            \ }

" ==================== GIT ====================
let g:gitgutter_realtime = 1
let g:gitgutter_eager = 1

function SwitchDiffW()
    if &diffopt =~ "iwhite"
        set diffopt-=iwhite
        let g:gitgutter_diff_args = ''
        GitGutterEnable
    else
        set diffopt+=iwhite
        let g:gitgutter_diff_args = '-w'
        GitGutterEnable
    endif
endfunction


"spell check when writing commit logs
autocmd filetype svn,*commit* setlocal spell

"http://vimcasts.org/episodes/fugitive-vim-browsing-the-git-object-database/
"hacks from above (the url, not jesus) to delete fugitive buffers when we
"leave them - otherwise the buffer list gets poluted
"
"add a mapping on .. to view parent tree
autocmd BufReadPost fugitive://* set bufhidden=delete
autocmd BufReadPost fugitive://*
  \ if fugitive#buffer().type() =~# '^\%(tree\|blob\)$' |
  \   nnoremap <buffer> .. :edit %:h<CR> |
  \ endif


